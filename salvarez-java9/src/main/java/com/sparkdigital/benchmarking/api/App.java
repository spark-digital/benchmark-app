package com.sparkdigital.benchmarking.api;

import static com.sparkdigital.benchmarking.api.RestPath.CPU_PATH;
import static com.sparkdigital.benchmarking.api.RestPath.DISK_PATH;
import static com.sparkdigital.benchmarking.api.RestPath.RAM_PATH;
import static com.sparkdigital.benchmarking.api.RestPath.THROUGHPUT_PATH;
import static spark.Spark.after;
import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.port;

import com.sparkdigital.benchmarking.api.service.BenchmarkService;
import com.sparkdigital.benchmarking.api.service.CPUService;
import com.sparkdigital.benchmarking.api.service.DiskService;
import com.sparkdigital.benchmarking.api.service.RAMService;
import com.sparkdigital.benchmarking.api.service.ThroughputService;

public class App {

	private static final String APPLICATION_JSON = "application/json";

	public static void main(String[] args) {

		BenchmarkService throughput = new ThroughputService();
		BenchmarkService cpu = new CPUService();
		BenchmarkService ram = new RAMService();
		BenchmarkService disk = new DiskService();

		port(80);

		get(THROUGHPUT_PATH, (req, res) -> {
			return execute(throughput);
		});
		get(CPU_PATH, (req, res) -> {
			return execute(cpu);
		});
		get(RAM_PATH, (req, res) -> {
			return execute(ram);
		});
		get(DISK_PATH, (req, res) -> {
			return execute(disk);
		});
		after((req, res) -> {
			res.type(APPLICATION_JSON);
		});
	}

	private static String execute(BenchmarkService service) {
		try {
			return service.process();
		} catch (Exception e) {
			halt(500, e.getMessage());
		}
		return null;
	}

}
