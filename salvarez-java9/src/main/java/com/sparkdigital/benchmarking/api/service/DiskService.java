package com.sparkdigital.benchmarking.api.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Service that reads a test file and dumps the content into a temporal file.
 * 
 * @author Soledad
 *
 */
public class DiskService implements BenchmarkService {

	private static final String PATH = "support/disk_test.csv";

	/**
	 * Returns a Json with the size of the test file
	 * @throws IOException 
	 */
	@Override
	public String process() throws IOException {
		long bytesCount = 0;
		byte[] bytes = Files.readAllBytes(Paths.get(PATH));
		createTempFile(bytes);
		bytesCount = bytes.length;
		return "{\"bytes\": " + bytesCount + "}\"";
	}

	private void createTempFile(byte[] bytes) throws IOException {
		File tempFile = File.createTempFile("disk_test" + java.util.UUID.randomUUID(), ".csv");
		Files.write(Paths.get(tempFile.getAbsolutePath()), bytes);
		tempFile.delete();
	}
}
