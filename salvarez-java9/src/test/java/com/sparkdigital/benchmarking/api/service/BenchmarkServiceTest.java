package com.sparkdigital.benchmarking.api.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.sparkdigital.benchmarking.api.service.BenchmarkService;
import com.sparkdigital.benchmarking.api.service.CPUService;
import com.sparkdigital.benchmarking.api.service.DiskService;
import com.sparkdigital.benchmarking.api.service.RAMService;
import com.sparkdigital.benchmarking.api.service.ThroughputService;

public class BenchmarkServiceTest {

    @Test
    public void throughputTest() {
        BenchmarkService throughput = new ThroughputService();
        Exception testException = null;
        String result = null;
        try {
        	result = throughput.process();
		} catch (Exception e) {
			testException = e;
		}
        assertNull(testException);
        assertNotNull(result);
        assertTrue(result.contains("throughput"));
	}

    @Test
    public void cpuTest() {
        BenchmarkService cpu = new CPUService();
        Exception testException = null;
        String result = null;
        try {
        	result = cpu.process();
		} catch (Exception e) {
			testException = e;
		}
        assertNull(testException);
        assertNotNull(result);
        assertTrue(result.contains("Hashed"));
	}

    @Test
    public void ramTest() {
        BenchmarkService ram = new RAMService();
        Exception testException = null;
        String result = null;
        try {
        	result = ram.process();
		} catch (Exception e) {
			testException = e;
		}
        assertNull(testException);
        assertNotNull(result);
        assertTrue(result.contains("n_vowels"));
	}
    
    @Test
    public void diskTest() {
        BenchmarkService disk = new DiskService();
        Exception testException = null;
        String result = null;
        try {
        	result = disk.process();
		} catch (Exception e) {
			testException = e;
		}
        assertNull(testException);
        assertNotNull(result);
        assertTrue(result.contains("bytes"));
	}
}
