'use strict';

const cpu = require('./cpu');
const disk = require('./disk');
const ram = require('./ram');
const throughput = require('./throughput');

module.exports = async function(req, res) {
  var result = null;
  console.log('entro una peticion!');
  switch (req.url) {
    case '/cpu':
      result = cpu();
      break;
    case '/disk':
      result = await disk();
      break;
    case '/ram':
      result = await ram();
      break;
    case '/throughput':
      result = throughput();
      break;
    default:
      res.writeHead(404);
      res.end();
      return;
  }

  res.end(JSON.stringify(result));
}