'use strict';

const os = require('os');
const fs = require('fs');
const path = require('path');

const FILEPATH = 'support/disk_test.csv';
const TMP_DIR = os.tmpdir();


module.exports = async function disk() {
  return new Promise((resolve, reject) => {
    var bytesCount = 0;
    var chunk = null;

    var tmpFilepath = path.join(TMP_DIR, new Date().getTime() + '' + Math.random());
    
    var readStream = fs.createReadStream(FILEPATH);
    var writeStream = fs.createWriteStream(tmpFilepath);
    
    readStream.pipe(writeStream);
    
    readStream.on('data', (chunk) => {
      bytesCount += chunk.length;
    });

    readStream.on('end', () => {
      fs.unlink(tmpFilepath, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve({
            bytes: bytesCount
          });
        }
      });
    });

  });
  
}